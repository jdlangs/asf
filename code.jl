module A

using PyPlot

import Base: +,-

immutable Pt
    x::Float64
    y::Float64
end

+(a::Pt, b::Pt) = Pt(a.x+b.x, a.y+b.y)
-(a::Pt, b::Pt) = Pt(a.x-b.x, a.y-b.y)
norm(p::Pt) = sqrt(p.x^2 + p.y^2)

function project(data)
    pts = Array(Pt, size(data,2))
    for i=1:size(data,2)
        r,θ = data[:,i]
        pts[i] = Pt(r*cos(θ), r*sin(θ))
    end
    return pts
end

function plotpts(pts, style="kx")
    xv = [pt.x for pt in pts]
    yv = [pt.y for pt in pts]
    plot(xv, yv, style)
end

function kmeans(pts, K)
    maxsize = 10.0
    means = [Pt(maxsize*rand(2)...) for i=1:K]
    converged = false
    while ! converged
        @show means
        sets = partition(pts, means)
        new_means = centroids(sets)
        dmeans = new_means - means
        if all(m -> norm(m) < 1e-4, dmeans)
            converged = true
        end
        means = new_means
    end
    return means
end

function partition(pts, means)
    sets = Array(Vector{Pt}, length(means))
    closest_idx = map(pt -> findclosest(pt, means), pts)
    for i=1:length(means)
        idx = find(x -> x == i, closest_idx)
        sets[i] = pts[idx]
    end
    return sets
end

function findclosest(pt, means)
    dists = [norm(pt - m) for m in means]
    return indmin(dists)
end

function centroids(sets)
    cntrs = Array(Pt, length(sets))
    for i=1:length(sets)
        N = length(sets[i])
        setsum = sum(sets[i])
        cntrs[i] = Pt(setsum.x/N, setsum.y/N)
    end
    return cntrs
end

#end

immutable Transform
    x::Float64
    y::Float64
    θ::Float64
end

function transform(pt::Pt, t::Transform)
    xt = pt.x*cos(t.θ) - pt.y*sin(t.θ) + t.x
    yt = pt.x*sin(t.θ) + pt.y*cos(t.θ) + t.y
    return Pt(xt,yt)
end

function transform(pts::Vector{Pt}, t::Transform)
    return map(pt -> transform(pt, t), pts)
end

function plotmeans(pts, means)
    figure()
    if length(means) == 3
        sets = partition(pts, means)
        ptsty = ["rx", "gx", "bx"]
        msty = ["ro", "go", "bo"]
        for i=1:3
            plotpts(sets[i], ptsty[i])
            plot(means[i].x, means[i].y, msty[i], markersize=10)
        end
    else
        plotpts(pts, "kx")
        for i=1:length(means)
            plot(means[i].x, means[i].y, "bo", markersize=10)
        end
    end
end

function gendata(N,Nc,dim)
    sigmas = 1.5*rand(Nc,2) .+ 0.5
    cntrs = [Pt(dim*rand(2)...) for i=1:Nc]
    @show cntrs
    X = zeros(N*Nc,2)
    for i=1:Nc
        idx = (i-1)*N+1:i*N
        X[idx,:] = hcat(sigmas[i,1]*randn(N)+cntrs[i].x, sigmas[i,2]*randn(N)+cntrs[i].y)
    end
    return X
end

function topts(X)
    [Pt(X[i,1],X[i,2]) for i=1:size(X,1)]
end

function topolar(X)
    r = sqrt(X[:,1].^2 + X[:,2].^2)
    θ = atan2(X[:,2],X[:,1])
    [r θ]
end

end
