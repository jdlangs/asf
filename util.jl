function makemap(N, density)
    Mv = rand(N,N)
    M = zeros(Int,N,N)

    for j=1:N, i=1:N
        M[i,j] = Mv[i,j] > density ? 0 : 1
    end
    M
end

function plotmap(M)
    dx = 1.0
    dy = 1.0
    axis([0,dx*size(M,1),0,dy*size(M,2)])
    for i=1:size(M,1)
        for j=1:size(M,2)
            if M[i,j] == 1
                x = dx*[i-1, i, i, i-1, i-1]
                y = dy*[j-1, j-1, j, j, j-1]
                plot(x,y,"k-")
            end
        end
    end
end

function plotpath(p)
    dx = 1.0
    dy = 1.0
    for i=2:length(p)
        plot([p[i-1].x, p[i].x] .- dx/2, [p[i-1].y, p[i].y] .- dx/2, "gs-")
    end
end

plotall(M,p) = (plotmap(M); plotpath(p))
