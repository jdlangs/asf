module ASF

using PyPlot

include("util.jl")

immutable Node
    x::Int
    y::Int
end

type NodeInfo
    g::Float64 #cost-to-come
    f::Float64 #start-to-goal cost through this node (estimated)
    parent::Node
end

const CONNECTIONS = [(1,0), (0,1), (-1,0), (0,-1)] #, (-1,-1), (-1,1), (1,1), (1,-1)]

manhattan(n1, n2) = abs(n1.x-n2.x) + abs(n1.y-n2.y)
euclidean(n1, n2) = norm([n1.x-n2.x, n1.y-n2.y])

function search(start::Node, goal::Node, map::Array, dist::Function, heuristic::Function=dist)
    closed_set = Set{Node}()
    open_set = Set{Node}([start])
    infos = Dict{Node,NodeInfo}()
    infos[start] = NodeInfo(0.0, heuristic(start, goal), Node(-1,-1))

    path = Node[]
    while length(open_set) > 0
        current = findbest(open_set, infos)
        open_set = delete!(open_set, current)
        if current != goal
            closed_set = push!(closed_set, current)
            neighbor_list = neighbors(current, map)
            neighbor_list = filter(n -> ! (n in closed_set), neighbor_list)
            for neighbor in neighbor_list
                g = infos[current].g + dist(current, neighbor)
                if ! (neighbor in open_set)
                    open_set = push!(open_set, neighbor)
                    infos[neighbor] = NodeInfo(Inf, Inf, current)
                end
                if g < infos[neighbor].g
                    f = g + heuristic(neighbor, goal)
                    infos[neighbor] = NodeInfo(g, f, current)
                end
            end
        else
            path = build_path(current, infos)
            return path
        end
    end
    return path
end

function findbest(set, nodeinfos)
    nodes = collect(set)
    scores = map(n -> nodeinfos[n].f, nodes)
    lidx = indmin(scores)
    return nodes[lidx]
end

function neighbors(node, map)
    nbrs = Node[]
    for (dx,dy) in CONNECTIONS
        nx = node.x + dx
        ny = node.y + dy
        if nx >= 1 && nx <= size(map,1) &&
           ny >= 1 && ny <= size(map,2) &&
           map[nx,ny] == 0
           push!(nbrs, Node(nx,ny))
       end
    end
    return nbrs
end

function build_path(n, nodeinfos)
    path = Node[]
    while n != Node(-1,-1)
        push!(path, n)
        n = nodeinfos[n].parent
    end
    return reverse(path)
end

function demo()
    M = makemap(30, 0.2)
    path = search(Node(1,1), Node(30,30), M, euclidean)

    figure()
    plotall(M,path)
end

end
